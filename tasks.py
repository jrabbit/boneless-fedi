from invoke import task
GOMOBILE_OPTS="-target=android -androidapi 26"
@task
def build(c):
    c.run(f"gomobile build {GOMOBILE_OPTS}", env={"ANDROID_NDK_HOME": "/home/jack/Downloads/android-ndk-r20b"}, echo=True)

@task
def install(c):
    c.run(f"gomobile install {GOMOBILE_OPTS}", env={"ANDROID_NDK_HOME": "/home/jack/Downloads/android-ndk-r20b"}, echo=True)
    
@task
def fuckxml(c):
    c.run("xmlformat --backup -i AndroidManifest.xml", echo=True)